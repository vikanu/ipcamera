# IPCamera Overview

Membuat project streaming ipcamera menggunakan lib Ozeki SDK dengan bahasa pemrograman C# menggunakan visual studio. Camera yang digunakan ipcamera dari hikvision.
Pada project ini terdapat beberapa Form utama antara lain : FormMain, Form Add, Form Connect. Form main merupakan dashboard sebagai tampilan utama dari program ini terdapat menu utama yaitu Add,Delete,Connect, Disconnect.

- Menu Add : digunakan untuk membuat object camera(panel camera).
- Menu Delete : digunakan untuk menghapus object camera(panel camera).
- Menu Connect : digunakan untuk menghubungkan atau koneksi camera ke program.
- Menu Disconnect : digunakan untuk memutus koneksi camera ke program.


Untuk menjalankan Menu Add, Delete, Connect, Disconnect         dilakukandengan cara dari Form Main klik kanan pada listBox camera. pilih option menu Add, Delete, Connect atau DisConnect.

Melakukan streaming ipcamera dengan program ini. menjalankan option add, untuk membuat object camera. jika sudah pada listBox camera terdapat object camera yang telah dibuat, object dengan nama Camera_0 dan tampil panel untuk streming camera pada Form Main. Jika ada object baru akan bertambah menjadi Camera_1, Camera_2 dst. Sebagai contoh kita sudah mempunyai object camera dengan nama Camera_0. selanjutnya kita akan melakukan koneksi, pada Camera_0 klik kanan pilih option Connect. 
Berikut ini penjelasan breakdown dari program:

## Breakdown   
- [FormMain](#Form-Main)
    - [Add](#Add)
    - [Create Obj Camera](#Create-Obj-Camera)
    - [Connect](#Connect)
    - [Disconnect](#Disconnect-Camera)
    - [Add Camera State Change to ListCamera](#Add-Camera-StateChange-to-ListCamera)
    - [Delete Camera By IDControl](#Delete-Camera-by-IDControl)

- [FormAdd](#Form-Add)
- [FormConnect](#Form-Connect)
- [Camera](#Camera)
    - [Camera Create Obj](#Camera-Create-Obj)
    - [Camera Obj Connect](#Camera-Obj-Connect)
    - [Camera Obj Disconnect](#Camera-Obj-Disconnect)
- [Panel User](#Panel-User)
    - [Add Panel](#Add-PanelUser)
    - [Add Camera to  User Control](#Add-Camera-to-User-Control)
    - [Remove Panel](#Remove-Panel)

- [Panel Menu Camera](#Panel-Menu-Camera)



## FormMain
### Add 
```csharp
method : 
void MenuStrip(object sender, EventArgs e)
                [parameter],[parameter]

method statement: //isi method add 
AddCamera = new frmAdd();   
AddCamera.ShowDialog();       

```
*Result :*
```csharp
((ToolStripItem).sender).Tag = "0";
//menu Add sender.Tag adalah 0

Show frmAdd --> class frmAdd.cs
```
Menu Strip Add, klik kanan pada list view. Tag( event Sender) menu add adalah 0. Menjalankan method diatas, sehingga tampil formAdd. AddCamera = new frmAdd() --> create object camera baru. AddCamera.ShowDialog()--> menampilkan formAdd.

### Create Obj Camera

```csharp
method : 
void MenuStrip(object sender, EventArgs e)
                [parameter],[parameter]

method statement: //isi method add 
Camera cam;
cam = new Camera();
```
*Result :*
```csharp
((ToolStripItem).sender).Tag = "0";
//menu Add sender.Tag adalah 0

Object Camera Name = cam
Property           = CameraName (string)

//call method Camera() in class Camera.cs
Camera() //class Camera.sc
{
    _Imageprovider = new DrawingImageProvider();
    _mediaConnector = new MediaConnector();
    _videoViewer = new VideoViewerWF();
}

```
pada formAdd(Tag Sender button adalah 0), berisi method cam = new Camera(), method ini memanggil method Camera() yang bera pada class Camera.cs
 yang berisi object2 untuk streming ipcamera seperti _Imageprovider,_mediaConnector, _videoViewer.

### Object Camera Name
```csharp
method : 
void MenuStrip(object sender, EventArgs e)
                [parameter],[parameter]

method statement: //isi method add 
cam.CameraName = "Camera_" + IDCamera.ToString();
```
*Result :*
```csharp
cam.CameraName = "Camera_0"
```
cam adalah object camera yang telah dibuat, ketika crete object pertama kali IDCamera adalah 0. Sehingga nama Camera menjadi "Camera_0"

### Add Obj Camera to UserControl
```csharp
method : 
void MenuStrip(object sender, EventArgs e)
                [parameter],[parameter]

method statement: //isi method  
((PanelUser)UsrCtrl1).AddPanel(cam);
```
*Result :*
```csharp
//call method in class PanelUser.cs
AddPanel(Camera cam)
[methodName][parameter]
``` 
method diatas berfungsi untuk memanggil method AddPanel() yang  terletak dalam class PanelUser.cs

### Add Camera StateChange to ListCamera
```csharp
method : 
void CameraStateChanged(string CameraName, CameraStateEventArgs e)
                        [parameter], [parameter State]

method statement: //isi method  
listBox1.Items.Add(CameraName + " : " + e.State.ToString());
```
*Result :*
```csharp
CameraName = "Camera_0"
e.State = "Streaming"
``` 
object cam yang telah dibuat, mempunyai bebrapa State, State disini merupakan keadaan mode camera, sebagai contoh ketika camera mulai melakukan connect obejct camera mempunyai mode Connected, ketika sudah tersambung mode berubah menjadi Connected, ketika sudah Stream mode berubah menjadi Streaming, dan terdapat mode Disconnected yang berarti tidak ada koneksi pada camera.
 method diatas untuk mengambil State camera. event e.State.ToString() dijadikan string agar bisa ditampilkan di listBox camera, listBox1.

### Connect
```csharp
method : 
void MenuStrip(object sender, EventArgs e)
                [parameter],[parameter]

method statement: //isi method Connect 
connnection = new frmConnect();
connnection.cameraActive = this.ActiveCamera;
connnection.Show();       

```
*Result :*
```csharp
((ToolStripItem).sender).Tag = "2";
//menu Connect sender.Tag adalah 2

Show frmConnect --> class frmConnect.cs
```
form Connect ini digunakan untuk memasukkan username, password, dan ipcamera yang akan digunakan untuk streaming Camera. untuk menampilkan form Add dikakukan dengan cara klik kanan pada listbox camera pilih option--> Connect.

Tag Sender ketika Connect adalah 2. Menjalankan method newFrmConnection(). ShowDialog(). method ini berfungsi untuk membuat object baru dari frmConnection dengan nama object Connection.

### Disconnect Camera
```csharp
method : 
void MenuStrip(object sender, EventArgs e)
                [parameter],[parameter]

method statement: //isi method Disconnect Camera 
this.ActiveCamera.Disconnect();       
```

*Result :*
```csharp
((ToolStripItem).sender).Tag = "3";
//menu Disconnect sender.Tag adalah 3

Call method --> void Disconnect() in class Camera.cs
```
Disconnect ini digunakan untuk memutus streaming Camera. untuk menampilkan Option Disconnect dikakukan dengan cara klik kanan pada listbox camera pilih option--> Disconnect.

Tag Sender ketika Disconnect adalah 3. Menjalankan method this.ActiveCamera.Disconnect(). 

### Delete Camera by IDControl
```csharp
method : 
void MenuStrip(object sender, EventArgs e)
                [parameter],[parameter]

method statement: //isi method Delete 
((PanelUser)UsrCtrl1).RemovePanel(ActiveCamera.IDControl);
ListViewItem item;
item = FindItemWithTag(lvCameraList, this.ActiveCamera);
if (item != null)
    {
        lvCameraList.Items.Remove(item);
    }

```
*Result :*
```csharp
((ToolStripItem).sender).Tag = "1";
//menu Delete sender.Tag adalah 1

ActivateCamera  = "Camera_0"
RemovePanel dengan ID COntrol = 0
item = {"Camera_0"}
//hapus dari listView Camera dengan text item "Camera_0"
lvCameraList.Items.Remove(item);
```
Delete ini digunakan untuk panel camera. untuk menampilkan Option Delete dikakukan dengan cara klik kanan pada listbox camera pilih option--> Delete.

Tag Sender ketika Delete adalah 1. ketika klik Delete maka akan menjalankan method diatas. code RemovePanel(ActiveCamera.IDControl), delete panel dengan berdasarkan idControlnya. 

diawal kita sudah punya 1 koneksi kamera dengan nama Camera_0 pada listBox camera.  pada menu delete, Camera_0 akan terhapus beserta panel cameranya.

## Form Add
### Form Add

| Var           | Type            | Desc              |
| :------------ | :---------------| :-----------------|
| Addr          | string          | alamat ipcamera   |
| Username      | string          | username camera   |
| Password      | string          | password camera   |

```csharp
method : 
void button1_Click(object sender, EventArgs e)
                [parameter],[parameter]

method statement: //isi method Delete 
Addr = txtAddress.Text;
UserName = txtUsername.Text;
Password = txtPassword.Text;
dgResult = DialogResult.OK;
Close();

```
*Result :*
```csharp
Addr    = "192.168.7.36:8888"
Username = "admin"
Password = "admin123"
```
## Form Connect
### Form Connect
```csharp
method : 
void button1_Click(object sender, EventArgs e)
                [parameter],[parameter]

method statement: //isi method Connect 
if (cameraActive != null)
    cameraActive.Connect(txtAddress.Text, txtUsername.Text, txtPassword.Text);

Close();

```
*Result :*
```csharp
call method -> void Connect(string Domain, string userName, string Password) in class Camera.cs



result method : 
Domain       = "192.168.7.36:8888"
userName     = "admin"
Password     = "admin123"
```
pada form connect isikan username, password dan ipcamera. klik tombol OK. maka akan memangggil fungsi Connect pada class Camera.cs 

## Camera
### Camera Create Obj
```csharp
Camera cam;
cam = new Camera();
cam.CameraName = "Camera_" + IDCamera.ToString();
```
*Result :*
```csharp
cam {
    
    CameraName = "Camera_0"
    IDControl = 0
    PanelViewer = null
    StateActive = NonActive
    _ImageProvider = {Ozeki.Media.DrawingImageProvider}
    _mediaConnector = {Ozeki.Media.MediaConnector}
    _videoViewer = {Ozeki.Media.MediaViewerWF}
}
```

### Camera Obj Connect
```csharp
method : void Connect(string Domain, string userName, string Password)
                        [parameter1],[parameter2],[parameter3]

method statement :
_camera = IPCameraFactory.GetCamera(Domain, userName, Password);                        
```

*Result :*
```csharp
parameter1 = "192.168.7.36:8888"
parameter2 = "admin"
parameter3 = "admin123"

```

### Camera Obj Disconnect 
```csharp
method : void Disconnect()

method statement :
_camera.CameraStateChanged -= _CameraStateChanged;
_camera.Disconnect();
_mediaConnector.Disconnect(_camera.VideoChannel, _Imageprovider);
_camera.Dispose();
_camera = null;
```
*Result :*
```csharp
CameraState :
CameraName = "Camera_0"
State = "Disconnected"
_camera = null
```

## Panel User
### Add PanelUser
```csharp
Meethod : AddPanel(Camera cam)
          {methodName}[parameter]  

```
*RESULT*

```csharp
((PanelMenuCamera)UsrCtrl1).IDControl = "0"
 UsrCtrl1.BackColor = "DarkGrey"
 cam.IDControl = "0";
```
### Add Camera to  User Control
```csharp
Method : AddPanel(Camera cam) 
parameter : cam
-----------
Method Statement :
UserControl UsrCtrl1 = new PanelMenuCamera();
((PanelMenuCamera)UsrCtrl1).IDControl = IDControl;
UsrCtrl1.BackColor = Color.DarkGray;
cam.IDControl = IDControl;
((PanelMenuCamera)UsrCtrl1).SetCameraProperty(cam);
```
*RESULT*
```csharp
cam.IDControl = 0

SetCameraProperty{
    lblMenu.Text = "Camera_0"
}
```
### Remove Panel
```csharp
Method : RemovePanel(int IDPanel)
         {MethodName}[parameter]

```
*RESULT*

```csharp
ActivateCamera  = "Camera_0"
RemovePanel dengan ID COntrol = 0
item = {"Camera_0"}
//hapus dari listView Camera dengan text item "Camera_0"
lvCameraList.Items.Remove(item);
```
## Panel Menu Camera

```csharp
Methhod : SetCameraProperty(Camera name)
parameter : name --> CameraName
```

*RESULT*
```csharp
name.CameraName = "Camera_0"
```

## Akses IPCamera
Akses ipcamera menggunakan Library Ozeki Camera SDK dan menggunakan local jaringan.

 - Setting ipcamera

    | Hardware           | Name           | IP              |Username        | Password          | Port      |
    | ------------------ | :------------- | :---------------|:---------------| :-----------------|:----------|
    | Dome Camera        | camera 1       | 192.168.7.37    | admin          | 12345hik        | 8888        |
    | Network Camera     | camera 2       | 192.168.7.36    | admin          | admin123          | 8888        |


 - Setting ip pada komputer

    | Name           | IP              |
    | :------------- | :---------------|
    | PC-1           | 192.168.7.35    | 

   



## Run SourceCode
untuk menjalankan source code diatas install visual studio min ver. visual studio 2015. 

#### Packages
`OzekiCamera SDK`

> `using Ozeki.Camera`

> `using Ozeki.Media`

### Installing
untuk menjalankan program, install file `setup1.msi` atau `setup.exe` yang terletak di folder Installer.

### Panduan Program

Sebelum menjalankan program, pastikan terlebih dahulu ip komputer dan ipcamera berada dalam satu jaringan.

1. Buka program yang telah diinstall. Berikut ini adalah tampilan halaman utama.

    ![Main Form](Images/Main.PNG)



2.  Add ipcamera. Klik kanan --> Add.

    ![Addip](Images/Addcamera.PNG). 

 
3. Masukkan ipcamera,username dan password --> klik button `Add` 

    ![ipcamera](Images/ipcamera.PNG)

4. Setelah add camera berhasil, akan muncul panelviewer dengan title camera_0 seperti gambar dibawah ini.

    ![Panelviewer](Images/Panelviewer.PNG) 


5. Pada List Camera Klik kanan --> Connect untuk mulai streaming 

    ![Connectcamera](Images/Connectcamera.PNG)

6. Masukkan ipcamera,username dan password --> klik button `Connect` 

    ![Connected](Images/Connected.PNG)

7. Berikut ini adalah hasil streaming ipcamera. Camera dalam mode streaming.

    ![Stream](Images/Stream.PNG)

8. Lakukan langkah berikut untuk Disconnect camera.

    ![Disconnect](Images/Disconnect.PNG)    

9. Lakukan langkah berikut untuk menghapus panelViewer Camera.

    ![Delete](Images/Delete.PNG) 


#### Alur Program
Berikut ini merupakan breakdown dari program.

![Stream](Images/Alurprogram.PNG)

##### ket :

1. [Main Form](https://bitbucket.org/vikanu/ipcamera/src/master/Documentation/FormMain.md)

2. [Add Camera](https://bitbucket.org/vikanu/ipcamera/src/master/Documentation/AddCamera.md)

3. [Connect Camera](https://bitbucket.org/vikanu/ipcamera/src/master/Documentation/ClassConnect.md)

4. [Disconnect Camera](https://bitbucket.org/vikanu/ipcamera/src/master/Documentation/Disconnect.md)

5. [Delete Camera](https://bitbucket.org/vikanu/ipcamera/src/master/Documentation/Delete.md)

#### Hardware :
* Dome Camera (PTZ Camera) - HIK
* VF IR Bullet Network Camera - HIK

#### Softaware :
* Windows 7 / win 10
* Visual Studio 2015 (minimal)


#### Hasil : 

* [Move Direction](https://bitbucket.org/vikanu/ipcamera/src/master/Documentation/PTZ.md) Dome Camera

* Dome Camera(Camera PTZ) Fungsi PTZ (Right, DownRight, Down, DownLeft, Left, UpLeft, Up, UpRight): button yang diatas dicoba sudah bisa, camera gerak sesuai button direction.
* Dome Untuk Camera PTZ tidak bisa load gambar pada form.
* Untuk menampilkan 2 kamera/ lebih pada 1 project, masih tetap belum bisa.