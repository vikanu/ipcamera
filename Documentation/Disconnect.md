 menu strip add `form1.cs` memanggil function ` public void Disconnect()` pada class `camera.cs`

 ```csharp
 //class form1.cs

 case 3://Disconnect
                    this.ActiveCamera.Disconnect();
                    break;
```
```csharp
//class camera.cs
        public void Disconnect()
        {
            _camera.CameraStateChanged -= _CameraStateChanged;
            _camera.Disconnect();
            _mediaConnector.Disconnect(_camera.VideoChannel, _Imageprovider);
            _camera.Dispose();
            _camera = null;
        }
```
