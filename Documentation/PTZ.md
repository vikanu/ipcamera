Tag button event `private void MouseDownMove(object sender, MouseEventArgs e)`

```csharp
private void MouseDownMove(object sender, MouseEventArgs e)
        {
            var button = sender as Button;
            var moveDirection = (MoveDirection)((Button)sender).Tag;

            if (ActiveCamera != null)
            {
                ActiveCamera.Move(moveDirection);

                listBox1.Items.Add(ActiveCamera.CameraName + " : Direction Move " + moveDirection.ToString());
            }
        }
```