public void AddPanel(Camera cam)
        {
            UserControl UsrCtrl1 = new PanelMenuCamera();
            ((PanelMenuCamera)UsrCtrl1).IDControl = IDControl;
            UsrCtrl1.BackColor = Color.DarkGray;
            cam.IDControl = IDControl;
            ((PanelMenuCamera)UsrCtrl1).SetCameraProperty(cam);


            UsrCtrl1.Location = new Point(LocationX, LocationY);

            if ((UsrCtrl1.Right + 20) >= this.Width)
            {
                LocationY = UsrCtrl1.Location.Y + UsrCtrl1.Height + 10;
                LocationX = 10;
            }
            else
                LocationX = UsrCtrl1.Location.X + UsrCtrl1.Width + 10;

            //UsrCtrl1.Width = this.Width - 50;

            pnlSlide.Controls.Add(UsrCtrl1);

            if (UsrCtrl1.Bottom >= this.Height)
                this.Height += 275;

            this.Refresh();

            IDControl++;
        }

code diatas digunakan sebagai pengaturan posisi add new camera. posisi kamera baru yang ditambahkan akan berada disebalah kanan.