Akses ipcamera dalam satu jaringan network
- Setting IP Address

    | Name           | IP              |
    | :------------- | :---------------|
    | PC-1           | 192.168.7.35    | 
    | camera-1       | 192.168.7.36    |
    | camera-2       | 192.168.7.37    |

- Cek koneksi antara IP, dengan command prompt

    ![testIP](../Images/testip.PNG)

