menu strip connect `form1.cs` memanggil event `button1_click` pada `frmConnect.cs`

```csharp
//tag menu strip Connect pd form1
case 2: //Connect
                    connnection = new frmConnect();
                    connnection.cameraActive = this.ActiveCamera;
                    connnection.Show();
```

```csharp
//event pd frmConnect.sc
if (cameraActive != null)
                cameraActive.Connect(txtAddress.Text, txtUsername.Text, txtPassword.Text);

            Close();
```

code diatas digunakan untuk konesksi camera, dilakukan pengecekan, jika camera aktif maka koneksi camera menggunakan ipaddress, username dan password.

    ipcamera : 192.168.7.36
    username : admin
    password : admin123

    ip camera : 192.168.7.37
    username : admin
    password : hik12345

`cameraActive.Connect`memanggil function `public void Connect(string Domain, string userName, string Password)` pada class `camera.cs`

```csharp
public void Connect(string Domain, string userName, string Password)
        {
            if (_camera != null)
            {
                _camera.CameraStateChanged -= _CameraStateChanged;
                _camera.Disconnect();
                _mediaConnector.Disconnect(_camera.VideoChannel, _Imageprovider);
                _camera.Dispose();
                _camera = null;
            }
        

            //_camera = IPCameraFactory.GetCamera("192.168.7.37:80", "admin", "12345hik");
            _camera = IPCameraFactory.GetCamera(Domain, userName, Password);
            //stateCamera
            _camera.CameraStateChanged += _CameraStateChanged;
            _mediaConnector.Connect(_camera.VideoChannel, _Imageprovider);
            //_connector.Connect(_camera.VideoChannel, _snapShot);


            _camera.Start();
            _videoViewer.Start();
        }
```

tampilan program : 
![Stream](/Images/Stream.PNG)
