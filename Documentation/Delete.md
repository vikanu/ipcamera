 menu strip Delete `form1.cs` memanggil function `public void RemovePanel(int IDPanel)`

 ```csharp

case 1: //Delete
                    if (ActiveCamera != null)
                    {
                        ((PanelUser)UsrCtrl1).RemovePanel(ActiveCamera.IDControl);

                        ListViewItem item;
                        item = FindItemWithTag(lvCameraList, this.ActiveCamera);
                        if (item != null)
                        {
                            lvCameraList.Items.Remove(item);
                        }
                    }
                    break;
```
```csharp
//class paneluser.cs
        public void RemovePanel(int IDPanel)
        {
            //to remove control by ID
            foreach (UserControl item in pnlSlide.Controls)
            {
                if (((PanelMenuCamera)item).IDControl == IDPanel)
                {
                    pnlSlide.Controls.Remove(item);
                    break;
                }
            }
        }
```
